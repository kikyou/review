## webpack

- corss-env 区分测试和生产
- css-loader 
- style-loader 将处理好的 css 通过 style 标签的形式添加到页面上
- file-loader 解决图片引入问题，并将图片 copy 到指定目录，默认为 dist
- url-loader 解依赖 file-loader，当图片小于 limit 值的时候，会将图片转为 base64 编码，大于 limit 值的时候依然是使用 - file-loader 进行拷贝
- img-loader 压缩图片
- babel-loader 
- cache-loader

- html-webpack-plugin 自动引入打包后的资源文件
- clean-webpack-plugin 打包之前自动清空目录
- mini-css-extract-plugin 通过 CSS 文件的形式引入到页面上
- uglifyjs-webpack-plugin
- terser-webpack-plugin
- speed-measure-webpack-plugin: 可以看到每个 Loader 和 Plugin 执行耗时 (整个打包耗时、每个 Plugin 和 Loader 耗时)
- webpack-bundle-analyzer
alias
reslove src
externals
include/exclude
noParse
IgnorePlugin
webpack-bundle-analyzer 
eval-cheap-module-source-map

## Loader 和 Plugin 的区别

Loader 本质就是一个函数，在该函数中对接收到的内容进行转换，返回转换后的结果。
因为 Webpack 只认识 JavaScript，所以 Loader 就成了翻译官，对其他类型的资源进行转译的预处理工作。
Plugin 就是插件，基于事件流框架 Tapable，插件可以扩展 Webpack 的功能，在 Webpack 运行的生命周期中会广播出许多事件，Plugin 可以监听这些事件，在合适的时机通过 Webpack 提供的 API 改变输出结果。

## vite

## 脚手架

- @kikyou/vscode-lint

## CI/CD

- gitlab

## 自己发布 npm 包

- @kikyou/vscode-lint
