## 逻辑像素，物理像素，像素比（dpr）等概念

-   物理像素，指设备显示屏上的像素点个数，例如常见的 1080 \* 1920
-   逻辑像素，虚拟像素
-   像素比，物理像素 = 逻辑像素\*像素比

## `viewport` 的理解

三个视口：

-   布局视口（Layout Viewport）默认为 980px，所以移动端一般可以显示完整 pc 端页面，只不过需要缩放。css 布局默认根据布局视口计算，可以通过 meta 标签来定义
-   视觉视口（Visual Viewport）我们能从屏幕中看到的视口
-   理想视口（Ideal Viewport）最理想的视口，无需缩放

## px，rem，em，vw，vh 等单位

-   `px` 逻辑像素的单位
-   `em` 指基于父元素的 `font-size` 相对大小
-   `rem` 指基于 `html` 的 `font-size` 的相对大小，可将 `html` 的 `font-size` 设为 `10px` 方便计算，即 `1rem = 10px`
-   `vw`可视视口宽度的 1%
-   `vh`可视视口高度的 1%

## web 移动端适配

-   百分比布局
-   `rem` 布局，根据页面分辨率去调整`html`的`font-size`大小
-   `vw`, `vh`
-   `rem + vw/vh` （推荐）
-   媒体查询`@media`

## 1px 问题

`1px`的线在 web 端正常，到了移动端成了 2px。

## 移动端 300ms 延迟

1. 通过 meta 标签禁用网页的缩放。
2. 通过 meta 标签将网页的 viewport 设置为 ideal viewport。
3. 调用一些 js 库，比如 FastClick
