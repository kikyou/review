# css

## 盒模型

![](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/0cc51f4ac8b64437ac30521bac4b4bf6~tplv-k3u1fbpfcp-zoom-in-crop-mark:3024:0:0:0.awebp?)

## `link` 和 `@import` 有什么区别

- `link` 属于 HTML 标签，而 `@import` 是 `css` 提供的
- 页面被加载时，`link` 会同时被加载，而 `@import` 引用的 `css` 会等到页面被加载完再加载
- `@import` 只在 IE5 以上才能识别，而 `link` 无兼容问题
- `link` 方式的样式的权重高于 `@import` 的权重

## css 选择器与优先级

1. `id` 选择器（`#id`）
2. 类选择器（`.class`）
3. 标签选择器（`div，h1，p`）
4. 后代选择器（`h1 p`）内部所有的`p`
5. 相邻后代选择器（子）选择器（`ul>li`）只选择直接父元素为`ul`的`li`元素，不包括重孙元素
6. 兄弟选择器（`li~a`）选取指定元素之后的所有相邻兄弟元素
7. 相邻兄弟选择器（`li+a`）选取指定元素之后的紧接着的兄弟元素
8. 属性选择器（`a[rel="external"]`）
9. 伪类选择器（`a:hover`，`li:nth-child`）
10. 伪元素选择器（`::before`，`::after`）
11. 通配符选择器（`*`）

优先级由 5 位数字构成：

- `a` `!important`
- `b` 行内样式
- `c` `ID` 选择器的数量
- `d` 类，伪类，属性选择器的数量
- `e` 标签选择器和伪元素选择器的数量
- `f` 通配选择器、后代选择器、兄弟选择器；

最终优先级`value = abcde`，当优先级相同时，后定义的 `css` 生效

## 伪类和伪元素的区别

- 伪类用于描述某个元素处于某个状态，例如`:hover` `:link`
- 伪元素用于创建一些不在文档树的元素 `::brfore` `::after`

## 水平/垂直居中方案

```css
margin: 0 auto;

text-aligin: center;

position: absolute;
margin: auto;
top: 0;
left: 0;
bottom: 0;
right: 0;

position: absolute;
left: calc(50% - 50px);
top: calc(50% - 50px);

position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);

display: flex;
align-items: center;
justify-content: center;
```

## flex 相关

flex 容器

1. `flex-direction: row | row-reverse | column | column-reverse` 属性决定主轴的方向
2. `flex-wrap: nowrap | wrap | wrap-reverse` 定义如何换行
3. `flex-flow: <flex-direction> || <flex-wrap>` 简写
4. `justify-content: flex-start | flex-end | center | space-between | space-around` 定义项目在主轴上的对齐方式
5. `align-items: flex-start | flex-end | center | baseline | stretch` 定义项目在交叉轴上的对齐方式
6. `align-content: flex-start | flex-end | center | space-between | space-around | stretch` 定义多轴线的对齐方式

flex 子项目

1. `order` 定义顺序
2. `grow` 定义放大比例
3. `shrink` 定义缩小比例
4. `basis` 定义在主轴上占据的空间，当 `shrink` 不为 0 时，在空间不够的情况下仍会缩小，不会等于定义
5. `flex` 前三者的结合体
6. `self` 定义子项目自身的在交叉轴上的对齐方式，可替代 `align-items`

## `width: 100%` 和 `width: auto`的区别

- `width: 100%`会使元素`box`的宽度等于父元素的`content box`的宽度。
- `width: auto`会使元素撑满整个父元素，`margin`、`border`、`padding`、`content`区域会自动分配水平空间。
- 一句话解释就是`width: 100%`容易超出父元素

## 简单介绍使用图片 base64 编码的优点和缺点

`base64` 编码是一种图片处理格式，通过特定的算法将图片编码成一长串字符串，在页面上显示的时候，可以用该字符串来代替图片的 `url` 属性。

使用 `base64` 的可以减少一个图片的 `HTTP` 请求

使用 `base64` 的缺点是：

1. 根据 `base64` 的编码原理，编码后的大小会比原文件大小大 `1`/`3`，如果把大图片编码到 `html`/`css` 中，不仅会造成文件体积的增加，影响文件的加载速度，还会增加浏览器对 `html` 或 `css` 文件解析渲染的时间。

2. 使用 `base64` 无法直接缓存，要缓存只能缓存包含 `base64` 的文件，比如 `HTML` 或者 `CSS`，这相比域直接缓存图片的效果要差很多。

3. 兼容性的问题，`ie8` 以前的浏览器不支持。

## margin 合并

垂直方向的 `margin` 合并不仅适用于兄弟元素，还适用于父子元素。不过中间不能有 `padding` 、`border` 等阻隔。

形成条件：

- 必须是处于常规文档流（非 `float` 和绝对定位）的块级盒子，并且处于同一个 `BFC` 当中。
- 没有线盒，没有空隙，没有 `padding` 和 `border` 将他们分隔开

破解 `margin` 合并的方法：

- 破坏两个 `margin` 的连续性
- `BFC`

## BFC 是什么？

`BFC` 指的是块级格式化上下文，BFC 是一个独立的布局环境，其中的元素布局是不受外界的影响，反之亦然

`BFC`布局规则：

- 内部的 `Box` 会在垂直方向，一个接一个地放置。
- `Box` 垂直方向的距离由 `margin` 决定。属于同一个 `BFC` 的两个相邻 `Box` 的 `margin` 会发生重叠。每个盒子（块盒与行盒）的 `margin box` 的左边，与包含块 `border box` 的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此。
- `BFC` 的区域不会与 `float box` 重叠。
- `BFC` 就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
- 计算 `BFC` 的高度时，浮动元素也参与计算

`BFC`的形成条件：

- 根元素或包含根元素的元素
- `float` 的值不是 `none`。
- `position` 的值不是 `static` 或者 `relative`。
- `display` 的值是 `inline-block`、`flex`、
- `overflow` 的值不是 `visible`

## 浮动与清除浮动

元素浮动之前，也就是在标准流中，是竖向排列的，而浮动之后可以理解为横向排列。

清除浮动不会清除浮动，它只能影响被设置清除浮动的元素，其他元素该浮动还是会浮动。它定义的是不允许元素左/右侧有浮动的元素。所以谁想改变，谁就得自己挪窝

- 在最后一个子元素之后设置一个空元素或者伪元素设置`clear: both`
- 父元素 BFC

## 让页面里的字体变清晰，变细用 CSS 怎么做？

`-webkit-font-smoothing: antialiased`

## 如何让去除 inline-block 元素间间距？

- 去除代码内的空格
- margin 负值
- 不闭合标签
- 父元素 `font-size: 0`
- `letter-spacing` 负值

## 如何实现单行文本溢出的省略？

`text-overflow: ellipsis;`

## 两列自适应布局

> 一列由内容撑开，另一列撑满剩余宽度

- 左 `float: left` / `position: absolute` + 右 `BFC` / `margin-left`
- 右 `position: absolute` + `left`
- `flex` 左放大缩小全是 0，右 `auto`

## 三栏布局

> 两边固定宽度，中间自适应

- 流体布局 / BFC 布局
- flex 布局
- 圣杯布局
- 双飞翼布局
- 绝对定位布局

## 回流（`reflow`）和重绘（`repaint`）

- 回流：当渲染树的一部分或者全部因为元素的尺寸，布局，隐藏等改变而需要重新渲染
- 重绘：当渲染树中的一些元素需要更新属性，而这些属性只是影响元素的外观，风格，并不影响布局
- 回流必重绘，重绘不一定回流

回流的发生条件：

- 添加或删除可见的 `DOM` 元素
- 元素的位置发生变化
- 元素的尺寸发生变化（包括外边距、内边框、边框大小、高度和宽度等）
- 内容发生变化，比如文本变化或图片被另一个不同尺寸的图片所替代。
- 页面一开始渲染的时候（这肯定避免不了）
- 浏览器的窗口尺寸变化（因为回流是根据视口的大小来计算元素的位置和大小的）

## 如何减少重绘和回流的次数？

- 避免一条一条的改变样式，使用 `cssText` 或者改变 `class` 代替之
- `CSS3`硬件加速，可以让`transform`等动画不触发回流和重绘。但是会导致内存占用加大
- 避免在多次直接访问元素的一些属性，而是缓存起来
- 让`DOM`脱离文档流：

  1. 可以将需要多次修改的 `DOM` 元素设置 `display: none`，操作完再显示
  2. 使用`DocumentFragment`将需要多次修改的 DOM 元素缓存，最后一次性 `append` 到真实 DOM 中渲染
  3. 绝对定位

## 层叠上下文

![](https://s.poetries.work/gitee/2020/09/111.png)
