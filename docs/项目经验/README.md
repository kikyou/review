## 自我介绍

面试官你好，我叫 xx,19 年毕业于 xx 大学，到现在差不多是三年经验。之前是在 xx 公司主要负责移动端项目的开发，包括微信网厅和小程序之类，主要用的技术栈是 vue2，自己也在空闲时间也学习过 vue3 相关的生态。

## 后台管理系统

登录

- 登录成功后 `token` 存储在 `sessionStorage` 里面
- 根据 `token` 获取用户信息

权限

分为页面权限和按钮权限两个。
通过`addRoutes`动态添加路由。
按钮权限则在指令中判断

- webpack-bundle-analyzer
- process.env
- mock

datepicker

pickerOptions

## 小程序

懒加载
lower-threshold lazy-load

## 说说你在项目中遇到的难点，以及怎么解决的？

- 构建流水线优化
- 前端错误监控
