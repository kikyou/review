## 前端常用的登录方式以及优缺点

## 如何一次渲染 100 万条数据

## 图片懒加载

```js
// 方法一
// 图片全部加载完成后移除事件监听；
// 加载完的图片，从 imgList 移除；
img.getBoundingClientRect().top < window.innerHeight;

// 方法二
const arr = [...document.querySelectorAll('img')];
const io = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      const img = entry.target;
      img.src = img.dataset.src;
      io.unobserve(img);
    }
  });
});
arr.forEach(entry => {
  io.observe(entry);
});
```
