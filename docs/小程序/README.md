## 小程序文件类型

- wxml
- wxss
- json
- js

## 登录流程

## 小程序的双向绑定和 vue 哪里不一样

`this.setData({})`

## 小程序页面间有哪些传递数据的方法?

- 全局 global data 然后通过 getApp()
- 跳转时在 url 后拼接参数，然后在 onload 接受

## 生命周期

- onload
- onshow
- onReady
- onHide
- onUnload

## 如何下拉刷新

首先配置 enablePullDownRefresh
onPullDownRefresh 钩子刷新
返回数据后 wx.stopPullDownRefresh
