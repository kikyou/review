# `VUE`

## `MVVM` 的理解

全称：Model-View-ViewModel

Model 表示数据模型层。 view 表示视图层， ViewModel 是 View 和 Model 层的桥梁，数据绑定到 viewModel 层并自动渲染到页面中，视图变化通知 viewModel 层更新数据。

## 响应式原理

- 2.x `Object.defineProperty`
- 3.x `proxy`

## 组件通信方式

- `props $emit`
- `$refs / $parent / $children`
- `eventBus / mitt`
- `$attrs / $listeners`
- `provide / inject`
- `vuex / pinia`

## `v-if` 和 `v-for` 优先级

- vue2 中，`v-for` 优先级高于 `v-if`，`v-if` 将依次判断每一个 `v-for` 的循环项
- vue3 中，`v-if` 优先级高于 `v-for`

## 生命周期

- `beforeCreate` `data` 和 `methods` 都还未初始化
- `created` `data` 和 `methods` 好了，但是模版尚未编译
- `beforeMount` 完成了模版编译，但是还未挂载在页面上
- `mounted` 都好了
- `beforeUpdate` `data` 已改变，但是还未渲染到页面上
- `updated` 已渲染到页面上
- `beforeDestroy / beforeUnmount` 实例销毁之前
- `destroyed / unmounted` 实例已经销毁
- `activited` 组件被激活时调用
- `deactivated` 组件被销毁时调用

## 双向绑定原理

`:value="val"` 和 `@input="val = $event.target.value"` 结合起来的语法糖

## `computed` 和 `watch` 区别

`computed`:

1. 监控自定义的变量，这个变量不可以和 `data、props` 里面的变量重复；
2. computed 属性的属性值是函数默认走 get 方法（必须有返回值），属性都有一个 `get` 和 `set` 方法；
3. 支持缓存，只有所依赖的数据发生变化才会重新计算，否则会取缓存中的数据；
4. 不支持异步，在 `computed` 里面操作异步无效；

`watch`:

1. 监听 `data、props` 里面数据的变化；
2. 不支持缓存，每次都会重新计算；
3. 支持异步，监听的函数接收两个参数，第一个参数是最新的值；第二个参数是输入之前的值；

## `keep-alive`

`keep-alive` 是 Vue 内置的一个组件，可以使被包含的组件保留状态，避免重新渲染 ，其有以下特性：

- 一般结合路由和动态组件一起使用，用于缓存组件
- 提供 `include` 和 `exclude` 还有 `max` 属性，两者都支持字符串或正则表达式， `include` `表示只有名称匹配的组件会被缓存，exclude` 表示任何名称匹配的组件都不会被缓存 ，其中 `exclude` 的优先级比 `include` 高。`max` 则规定了最大缓存的组件数量
- 对应两个钩子函数 `activated` 和 `deactivated` ，当组件被激活时，触发钩子函数 `activated`，当组件被移除时，触发钩子函数 `deactivated`

## `$nextTick`

nextTick 是等待下一次 DOM 更新刷新的工具方法
Vue 更新策略是异步的，会将所有更改数据的操作保存在一个队列中。所以更改数据后并不能马上获取 dom 的最新状态。当需要获取数据改变后，使用此函数获取更新后的 DOM

## 虚拟 `DOM`

虚拟 DOM 的实现原理主要包括以下 3 部分：

用 JavaScript 对象模拟真实 DOM 树，对真实 DOM 进行抽象；
diff 算法 — 比较两棵虚拟 DOM 树的差异；
pach 算法 — 将两个虚拟 DOM 对象的差异应用到真正的 DOM 树。

- 优点：

  保证性能下限： 框架的虚拟 DOM 需要适配任何上层 API 可能产生的操作，它的一些 DOM 操作的实现必须是普适的，所以它的性能并不是最优的；但是比起粗暴的 DOM 操作性能要好很多，因此框架的虚拟 DOM 至少可以保证在你不需要手动优化的情况下，依然可以提供还不错的性能，即保证性能的下限；
  无需手动操作 DOM： 我们不再需要手动去操作 DOM，只需要写好 View-Model 的代码逻辑，框架会根据虚拟 DOM 和 数据双向绑定，帮我们以可预期的方式更新视图，极大提高我们的开发效率；
  跨平台： 虚拟 DOM 本质上是 JavaScript 对象,而 DOM 与平台强相关，相比之下虚拟 DOM 可以进行更方便地跨平台操作，例如服务器渲染、weex 开发等等。

- 缺点:

  无法进行极致优化： 虽然虚拟 DOM + 合理的优化，足以应对绝大部分应用的性能需求，但在一些性能要求极高的应用中虚拟 DOM 无法进行针对性的极致优化。

## `diff` 算法

这个懒得看了

## `new Vue()` 之后发生了什么

![](https://s.poetries.work/gitee/2020/07/vue/1.png)

## `SPA` 优缺点

SPA 不会因为用户的操作而进行页面的重新加载或跳转；取而代之的是利用路由机制实现 HTML 内容的变换，UI 与用户的交互，避免页面的重新加载。

- 优点：

  - 用户体验好、快，内容的改变不需要重新加载整个页面，避免了不必要的跳转和重复渲染；
  - 基于上面一点，SPA 相对对服务器压力小；
  - 前后端职责分离，架构清晰，前端进行交互逻辑，后端负责数据处理；

- 缺点：

  - 初次加载耗时多：为实现单页 Web 应用功能及显示效果，需要在加载页面的时候将 JavaScript、CSS 统一加载，部分页面按需加载；
  - 前进后退路由管理：由于单页应用在一个页面中显示所有的内容，所以不能使用浏览器的前进后退功能，所有的页面切换需要自己建立堆栈管理；
  - SEO 难度较大：由于所有的内容都在一个页面中动态替换显示，所以在 SEO 上其有着天然的弱势。

## `SPA` 首页白屏

## 你对 axios 做了怎样的封装

- header 部分带 token
- 返回对状态码处理

## 路由生命周期

1. 导航被触发。
2. 在失活的组件里调用 beforeRouteLeave 守卫。
3. 调用全局的 beforeEach 守卫。
4. 在重用的组件里调用 beforeRouteUpdate 守卫
5. 在路由配置里调用 beforeEnter。
6. 解析异步路由组件。
7. 在被激活的组件里调用 beforeRouteEnter。
8. 调用全局的 beforeResolve 守卫
9. 导航被确认。
10. 调用全局的 afterEach 钩子。
11. 触发 DOM 更新。
12. 调用 beforeRouteEnter 守卫中传给 next 的回调函数，创建好的组件实例会作为回调函数的参数传入。

## 动态路由

`/user/:id` 注意冒号

## `hash` 和 `history` 模式有什么区别

- `hash` 模式

  `location.hash` 的值实际就是 URL 中 `#` 后面的东西 它的特点在于：

  - hash 虽然出现 URL 中，但不会被包含在 HTTP 请求中，对后端完全没有影响，因此改变 hash 不会重新加载页面
  - 使用`hashchange` 事件监听 `hash` 改变

    ```js
    window.addEventListener('hashchange', funcRef, false);
    ```

  - 每一次改变 hash（`window.location.hash`），都会在浏览器的访问历史中增加一个记录利用
  - 兼容性好但是不美观

- `history` 模式

  - 利用了 HTML5 中的 `pushState()` 和 `replaceState()` 方法

  - 这两个方法应用于浏览器的历史记录站，在当前已有的 `back、forward、go` 的基础之上，它们提供了对历史记录进行修改的功能
  - 我们可以使用 `popstate` 事件来监听 url 的变化，从而对页面进行跳转（渲染）；
  - 虽然美观，但是刷新会出现 404 需要后端进行配置

## vuex 如何动态注册模块？

`registerModule`
